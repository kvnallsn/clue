CC=javac
LDFLAGS=
SOURCES=Board.java Clue.java DetectiveNotes.java Green.java Mustard.java Peacock.java Plum.java Scarlet.java Square.java White.java
EXECUTABLE=clue

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(SOURCES) 

.java:
	$(CC) $< -o $@

clean:
	rm -r *.class
