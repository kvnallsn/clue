/*
 * CluePlayer.java
 *
 * Version:
 *    $Id$
 *
 * Revisions:
 *    &Log$
 *
 */

import java.util.*;

/** 
 * This class contains player strategies for the game of Clue. 
 *
 * @author     Kevin Allison 
 *
 */

public class CluePlayer {  

    private static final int INVALID = -1;
    private static final int UP = 0;
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int LOOK_AHEAD = 6;
    
    private Map<String, RoomInfo> room_map;
    
    public CluePlayer() {
        room_map = new HashMap<String, RoomInfo>();
    }

    /**
     *  Find a random square on the board for a player to move to.
     *
     *  @return                  The square that the player ends up on  
     */
    public Square findSquareRand() {

        int row = 0, col = 0;
        boolean valid = false;
        
        while (!valid) {
            col = (int)(Math.random()*(Clue.board.WIDTH)) + 1;
            row = (int)(Math.random()*(Clue.board.HEIGHT)) + 1;
            if (col >= 0 && col < Clue.board.WIDTH && 
                row >= 2 && row < Clue.board.HEIGHT)
                valid = true;
        }  
        return new Square(row, col);
    }

    /**
     * Search the given direction for a door
     *
     *   @param   dir         The direction to search (UP,DOWN,LEFT,RIGHT)
     *   @param   startSquare The square to start searching from
     *   @param   stopSquare  The square to stop search at
     *   @param   rc          The row or column to stay in (column for UP/DOWN, row for LEFT/RIGHT)
     *
     *   @return              The location of the door to move to, or -1 if no door found
     */
    private int checkForDoor(int dir, int startSquare, int stopSquare, int rc) {
       
        int retVal = -1;
        switch (dir) {
            case UP:
                for (int i = startSquare-1; i >= stopSquare; i--) {
                    if (Clue.board.isDoor(i, rc)) { retVal = i; break; }
                }
                break;
            case DOWN:
                for (int i = startSquare+1; i <= stopSquare; i++) {
                    if (Clue.board.isDoor(i, rc)) { retVal = i; break; }
                }
                break;
            case LEFT:
                for (int i = startSquare-1; i >= stopSquare; i--) {
                    if (Clue.board.isDoor(rc, i)) { retVal = i; break; }
                }
                break;
            case RIGHT:
                for (int i = startSquare+1; i <= stopSquare; i++) {
                    if (Clue.board.isDoor(rc, i)) { retVal = i; break; }
                }
                break;
        }

        return retVal;
    }

     /** Search the given direction for a room
     *
     *   @param   dir         The direction to search (UP,DOWN,LEFT,RIGHT)
     *   @param   startSquare The square to start searching from
     *   @param   stopSquare  The square to stop search at
     *   @param   rc          The row or column to stay in (column for UP/DOWN, row for LEFT/RIGHT)
     *
     *   @return              The location of the first square found that is not empty, or -1 if all squares valid
     */
    private int checkForRoom(int dir, int startSquare, int stopSquare, int rc) {

		int retVal = -1;
		
        switch (dir) {
            case UP:
                for (int i = startSquare-1; i >= stopSquare; i--) {
                    if (!Clue.board.getRoom(i, rc).equals(" ")) { retVal = i; break; }
                }
                break;
            case DOWN:
                for (int i = startSquare+1; i <= stopSquare; i++) {
                    if (!Clue.board.getRoom(i, rc).equals(" ")) { retVal = i; break; }
                }
                break;
            case LEFT:
                for (int i = startSquare-1; i >= stopSquare; i--) {
                    if (!Clue.board.getRoom(rc, i).equals(" ")) { retVal = i; break; }
                }
                break;
            case RIGHT:
                for (int i = startSquare+1; i <= stopSquare; i++) {
                    if (!Clue.board.getRoom(rc, i).equals(" ")) { retVal = i; break; }
                }
                break;
        }

        return retVal;
    }

    /**
     *  Check the provided path for a room
     *  Called when a player is going towards a door to ensure that
     *  a room isn't in the way
     *
     *  @param    dir            The direction to move
     *  @param    start_c        The start column
     *  @param    start_r        The start row
     *  @param    end_c          The end column
     *  @param    end_r          The end row
     * 
     *  @return                  True if a path is valid, false otherwise(Row/Col to end on if valid, -1 otherwise)
     */
     private int checkPath(int dir, int start_r, int start_c, int end_r, int end_c) {
        int room_loc = -2;
        int door_loc = -2;
        int retVal = -1;
        boolean stopAtDoor = true;
        
        if (Clue.board.isDoor(start_r, start_c)) { 
            // Starting on door must move out
            stopAtDoor = false;
        }
        
        if (dir == UP || dir == DOWN) {
            if (end_r < 2 || end_r > Clue.board.HEIGHT-1) { return retVal; }
            
            room_loc = checkForRoom(dir, start_r, end_r, start_c);
            door_loc = checkForDoor(dir, start_r, end_r, start_c);
            retVal = end_r;
            
        } else if (dir == LEFT || dir == RIGHT) {
            if (end_c < 0 || end_c > Clue.board.WIDTH-1) { return retVal; }
            
            room_loc = checkForRoom(dir, start_c, end_c, start_r);
            door_loc = checkForDoor(dir, start_c, end_c, start_r);
            retVal = end_c;
        }
        
        if ((room_loc > -1 && door_loc > -1) && (room_loc == door_loc)) {
            if (stopAtDoor) {
                retVal = door_loc;
            }
        } else if (room_loc > -1 || door_loc > -1) {
            retVal = -1;
        }
        
        return retVal;
     }

    /**
     *  Find a square on the board for a player to move to by rolling the 
     *  die and chosing a random direction. The square that is chosen must
     *  be legally accessible to the player (i.e., the player must adhere to 
     *  the rules of the game to be there).
     *
     *  @param    c_row          The current row of this player
     *  @param    c_col          The current column of this player
     *
     *  @return                  The square that this player ends up on
     */
    public Square findSquareRandDir(int c_row, int c_col) {
     
        int row = 0, col = 0;
        boolean valid = false;

        int dir  = 0;
        while (!valid) {
            dir = (int)(Math.random() * 4);
 
            row = dir == UP ? c_row - Clue.die : (dir == DOWN ? c_row + Clue.die : c_row);
            col = dir == LEFT ? c_col - Clue.die : (dir == RIGHT ? c_col + Clue.die : c_col);

            if (col >= 0 && col < Clue.board.WIDTH &&
                row >= 2 && row < Clue.board.HEIGHT) {

                int door = -1;
                if (dir == UP || dir == DOWN) { 
					door = checkForDoor(dir, c_row, row, c_col);
                    
                    door = door == c_row ? -1 : door;
                    row = door != -1 ? door : row;
                } else {
					door = checkForDoor(dir, c_col, col, c_row);
                    
                    door = door == c_col ? -1 : door;
                    col = door != -1 ? door : col;
                }
				
                int room = -1;
                if (door == -1) {
                    if (dir == UP || dir == DOWN) { 
                        room = checkForRoom(dir, c_row, row, c_col);
                    } else {
                        room = checkForRoom(dir, c_col, col, c_row);
                    }
					
                    if (room == -1) { valid = true; }
                } else {
                    // Check path for a room
                    valid = checkPath(dir, c_row, c_col, row, col) != -1 ? true : false;
                }

            }

            if (!valid) { Clue.die = (int)(Math.random() * 6) + 1; }
        }
        
        return new Square(row, col);
    }
    
    /**
     * Stores information about a given room
     * Used to generate valid routes to take
     * by examining what doors are avaliable
     */
    class RoomInfo {       
        int top_row;
        int bottom_row;
        int left_col;
        int right_col;
            
        class Door {
            public int row;
            public int col;
            
            public Door(int _row, int _col) { row = _row; col = _col; }
            
            @Override
            public boolean equals(Object obj) {
                return (this.row == ((Door)obj).row && this.col == ((Door)obj).col);
            }
            
            @Override
            public int hashCode() {
                return (row + (col * 10));
            }
        }
        
        public Set<Door> doors;
        
        /**
         * Scans a room for doors in it, then stores that information
         * in a Set<Door>
         *
         * @param   _row,_col         A (row, col) pair in the room to start the search from
         */
        public RoomInfo(int _row, int _col) {
            doors = new HashSet<Door>();
            
            // Set "walls"
            top_row = 2;
            bottom_row= Clue.board.HEIGHT - 1;
            left_col = 0;
            right_col = Clue.board.WIDTH - 1;
            
            for (int i = _row; i > 1; --i) {
                if (Clue.board.getRoom(i, _col).equals(" ")) {
                    top_row = i + 1;
                    break;
                }
            }
           
            for (int i = _row; i < Clue.board.HEIGHT; ++i) {
                if (Clue.board.getRoom(i, _col).equals(" ")) {
                    bottom_row = i - 1;
                    break;
                }
            }
            
            for (int i = _col; i > -1; --i) {
                if (Clue.board.getRoom(_row, i).equals(" ")) {
                    left_col = i + 1;
                    break;
                }
            }
           
            for (int i = _col; i < Clue.board.WIDTH; ++i) {
                if (Clue.board.getRoom(_row, i).equals(" ")) {
                    right_col = i - 1;
                    break;
                }
            }
            
            for (int i = top_row; i <= bottom_row; ++i) {
                if (Clue.board.isDoor(i, left_col)) {
                    doors.add(new Door(i, left_col));
                }
                
                if (Clue.board.isDoor(i, right_col)) {
                    doors.add(new Door(i, right_col));
                }
            }
            
            for (int i = left_col; i <= right_col; ++i) {
                if (Clue.board.isDoor(top_row, i)) {
                    doors.add(new Door(top_row, i));
                }
                
                if (Clue.board.isDoor(bottom_row, i)) {
                    doors.add(new Door(bottom_row, i));
                }
            }          
        }
    }
    
    /**
     *  Get a list of all the nearby rooms and the direction you would need to
     *  move in order to get there
     *
     *  @param    c_row          The current row of this player
     *  @param    c_col          The current column of this player
     *
     *  @return                  A map containing a mapping of rooms to their direction
     */
     private Set<String> getNearbyRooms(int c_row, int c_col, int search_size) {
     
        Set<String> nearby_rooms = new HashSet<String>();
        
        int top = 2;
        int bottom = Clue.board.HEIGHT - 1;
        int left = 0;
        int right = Clue.board.WIDTH - 1;
        
        for (int r = top; r <= bottom; r++) {
            for (int c = left; c <= right; c++) {
                if (!Clue.board.getRoom(r, c).equals(" ")) {
                    String room = Clue.board.getRoom(r, c);
                    if (!room_map.containsKey(room)) {
                        room_map.put(room, new RoomInfo(r, c));
                    }
                    nearby_rooms.add(room);
                }
            }
        }
        
        return nearby_rooms;
    }

    /**
     *  Find a square on the board for a player to move to by rolling the 
     *  die and chosing a good direction. The square that is chosen must
     *  be legally accessible to the player (i.e., the player must adhere to 
     *  the rules of the game to be there).
     *
     *  @param    c_row          The current row of this player
     *  @param    c_col          The current column of this player
     *  @param    notes          The Detective Notes of this player 
     *
     *  @return                  The square that this player ends up on
     */
    public Square findSquareSmart(int c_row, int c_col, DetectiveNotes notes) {

        int size_factor = 1;
        List<String> my_rooms = notes.getMyRooms();
        Set<String> nearby_rooms = getNearbyRooms(c_row, c_col, LOOK_AHEAD);
        while (nearby_rooms.size() == 0) {
            // Double the search size and try again
            size_factor *= 2;
            nearby_rooms = getNearbyRooms(c_row, c_col, LOOK_AHEAD * size_factor);
        }

        RoomInfo.Door closest_door = null;
        int door_dist = Clue.board.HEIGHT * Clue.board.WIDTH + 1;
        
        for (String room : nearby_rooms) {
            if (!my_rooms.contains(room)) {
                RoomInfo r = room_map.get(room);
                // Determine closet door
                for (RoomInfo.Door d : r.doors) {
                    int dist = Math.abs(d.row - c_row) + Math.abs(d.col - c_col);
                    if (dist < door_dist) {
                        door_dist = dist;
                        closest_door = d;
                    }
                }
            }
        }
        
        if (closest_door == null) { 
            RoomInfo r = room_map.get(Clue.board.getRoom(c_row, c_col));
            closest_door = r.doors.iterator().next();
        }
        
        int row = 2;
        int col = 0;
        int dir = INVALID;
        
        boolean valid = false;
        
        while (!valid) {
            // Try moving different options and pick the best result
            int up_r = c_row - Clue.die;
            int down_r = c_row + Clue.die;
            int left_c = c_col - Clue.die;
            int right_c = c_col + Clue.die;
            
            int up_s = -1;
            int down_s = -1;
            int left_s = -1;
            int right_s = -1;
            
            int final_s;
            
            int up_h = -1;
            int down_h = -1;
            int left_h = -1;
            int right_h = -1;
            
            int lowest_h = -1;
            
            // Check if path is valid, then calculate Heuristic (dist to door after move)
            
            if ((up_s = checkPath(UP, c_row, c_col, up_r, c_col)) != -1) {
                lowest_h = Math.abs(up_r - closest_door.row) + Math.abs(c_col - closest_door.col);
                dir = UP;
            } 
            
            if ((down_s = checkPath(DOWN, c_row, c_col, down_r, c_col)) != -1) {
                down_h = Math.abs(down_r - closest_door.row) + Math.abs(c_col - closest_door.col);
                if ((lowest_h != -1 && down_h < lowest_h) || lowest_h == -1) { dir = DOWN; lowest_h = down_h; }

            } 
            
            if ((left_s = checkPath(LEFT, c_row, c_col, c_row, left_c)) != -1) {
                left_h = Math.abs(c_row - closest_door.row) + Math.abs(left_c - closest_door.col);
                if ((lowest_h != -1 && left_h < lowest_h) || lowest_h == -1) { dir = LEFT; lowest_h = left_h; }
            } 
            
            if ((right_s = checkPath(RIGHT, c_row, c_col, c_row, right_c)) != -1) {
                right_h = Math.abs(c_row - closest_door.row) + Math.abs(right_c - closest_door.col);
                if ((lowest_h != -1 && right_h < lowest_h) || lowest_h == -1) { dir = RIGHT; lowest_h = right_h; }
            } 
                       
            // Find minimun heuristic
            if (dir == UP) {
                row = up_s;
                col = c_col;
                valid = true;
            } else if (dir == DOWN) {
                row = down_s;
                col = c_col;
                valid = true;
            } else if (dir == LEFT) {
                row = c_row;
                col = left_s;
                valid = true;
            } else if (dir == RIGHT) {
                row = c_row;
                col = right_s;
                valid = true;
            } else {
                Clue.die = (int)(Math.random() * 6) + 1;
            }
        }
     
        return new Square(row, col);
    }

    /**
     *  Move to a legal square on the board. If the move lands on a door,
     *  make a suggestion by guessing a random suspect and random weapon.
     *
     *  @param    curr_row        The row of the player before move
     *  @param    curr_column     The column of the player before move
     *  @param    row             Selected row 
     *  @param    column          Selected column 
     *  @param    color           Player color
     *  @param    name            Player name
     *  @param    notes           Player Detective Notes 
     *
     *  @return                   A suggestion -> [name,room,suspect,weapon]
     */
    public String[] moveNaive(int curr_row, int curr_column, 
                         int row, int column, String color, String name, 
                         DetectiveNotes notes) {

        String [] retVal = new String[4];
        String suspect = notes.getRandomSuspect();
        String weapon = notes.getRandomWeapon();
        String room = Clue.board.getRoom(row,column);

        if (Clue.board.isDoor(curr_row,curr_column))
            Clue.board.setColor(curr_row,curr_column,"Gray");
        else 
            Clue.board.setColor(curr_row, curr_column, "None");

        if (Clue.board.isDoor(row,column)) { 
            retVal[0] = name;
            retVal[1] = room;
            retVal[2] = suspect;
            retVal[3] = weapon;

            if (Clue.gui) {
                System.out.print(name+" suggests that the crime was committed");
                System.out.println(" in the " + room + " by " + suspect +
                               " with the " + weapon);
            }
        }
        else retVal = null;

        Clue.board.setColor(row,column,color);

        return retVal;
    }

    /**
     *  Move to a legal square on the board. If the move lands on a door,
     *  make a good suggestion for the suspect and the weapon. A good
     *  suggestion here is one which does not include any suspects or
     *  weapons that are already in the Detective Notes of this player.
     *
     *  @param    curr_row        The row of the player before move
     *  @param    curr_column     The column of the player before move
     *  @param    row             Selected row 
     *  @param    column          Selected column 
     *  @param    color           Player color
     *  @param    name            Player name
     *  @param    notes           Player Detective Notes 
     *
     *  @return                   A suggestion -> [name,room,suspect,weapon]
     */
    public String[] moveSmart(int curr_row, int curr_column, 
                         int row, int column, String color, String name, 
                         DetectiveNotes notes) {

	
        String [] retVal = new String[4];
        
        // Change this part
        String suspect = notes.getRandomSuspect();
        while (notes.getMySuspects().contains(suspect)) {
            suspect = notes.getRandomSuspect();
        }
        String weapon = notes.getRandomWeapon();
        while (notes.getMyWeapons().contains(weapon)) {
            weapon = notes.getRandomWeapon();
        }
        String room = Clue.board.getRoom(row,column);

        if (Clue.board.isDoor(curr_row,curr_column))
            Clue.board.setColor(curr_row,curr_column,"Gray");
        else 
            Clue.board.setColor(curr_row, curr_column, "None");

        if (Clue.board.isDoor(row,column)) { 
            retVal[0] = name;
            retVal[1] = room;
            retVal[2] = suspect;
            retVal[3] = weapon;

            if (Clue.gui) {
                System.out.print(name+" suggests that the crime was committed");
                System.out.println(" in the " + room + " by " + suspect +
                               " with the " + weapon);
            }
        }
        else retVal = null;

        Clue.board.setColor(row,column,color);
        
        return retVal;
    }
    
    /**
     *  Try to prove a suggestion is false by asking the players, in a
     *  round-robin fashion, to show the suggester one of the suggestions if
     *  the player has that suggested card in their hand. The other players 
     *  know that ONE of the suggestions cannot be in the case file, but they 
     *  do not know which one.
     *
     *  @param  suggestion      A suggestion -> [name,room,suspect,weapon]
     *  @param  notes           The Detective Notes of the current player
     *  @param  player          The current player
     *  @param  next            The next player clockwise around the board
     *  
     *  @return                 True if a player has won the game
     *
     */
    public boolean prove(String[] suggestion, DetectiveNotes notes,
                         int player, int next) {
        
        int p = next;
        boolean card_shown = false;
        while (p != player) {     
            
            if (Clue.allCards.get(p).containsKey(suggestion[1])) {
                // Room Found
                setNotes(notes, suggestion[1], Clue.allCards.get(p).get(suggestion[1]));
                card_shown = true;
                break;
            } else if (Clue.allCards.get(p).containsKey(suggestion[2])) {
                // Suspect Found
                setNotes(notes, suggestion[2], Clue.allCards.get(p).get(suggestion[2]));
                card_shown = true;
                break;
            } else if (Clue.allCards.get(p).containsKey(suggestion[3])) {
                // Weapon Found
                setNotes(notes, suggestion[3], Clue.allCards.get(p).get(suggestion[3]));
                card_shown = true;
                break;
            }
                    
            if (p >= Clue.allPlayers.size() - 1) { p = 0; } else { ++p; }
        }
        
        // Make sure current player doesn't have those cards
        if (notes.getMyRooms().contains(suggestion[1]) ||
            notes.getMySuspects().contains(suggestion[2]) ||
            notes.getMyWeapons().contains(suggestion[3])) {
            
            card_shown = true;
        }
        
        return !card_shown;
    }

    /**
     *  Update this player's detective notes upon learning some information.
     *
     *  @param    notes    The detective notes of this player
     *  @param    card     The card that caused the change
     *  @param    type     The type of the card - suspect, weapon, or room
     *
     */
    public void setNotes(DetectiveNotes notes, String card, String type) {

        if (type.equals("suspect"))
            notes.addSuspect(card);
        else if (type.equals("weapon"))
            notes.addWeapon(card);
        else if (type.equals("room"))
            notes.addRoom(card);
    }
}
